//Stream转MemoryStream

public static MemoryStream ConvertStreamToMemoryStream(Stream stream)
        { 
            MemoryStream memoryStream = new MemoryStream();
            if (stream != null)
            {
                byte[] buffer = ReadFully(stream);
                if (buffer != null)
                {
                    var binaryWriter = new BinaryWriter(memoryStream);
                    binaryWriter.Write(buffer);
                } 
            } 
            return memoryStream;
        }