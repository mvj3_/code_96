//IrandomAccessStream转Ibuffer

Stream stream=WindowsRuntimeStreamExtensions.AsStreamForRead(randomStream.GetInputStreamAt(0));
MemoryStream memoryStream = new MemoryStream();            
if (stream != null)
{
    byte[] bytes = ReadFully(stream);
    if (bytes != null)
    {
        var binaryWriter = new BinaryWriter(memoryStream);
        binaryWriter.Write(bytes);
    }
} 
IBuffer buffer=WindowsRuntimeBufferExtensions.GetWindowsRuntimeBuffer(memoryStream,0,(int)memoryStream.Length);